import Linkers.Linker;
import Linkers.OFPLinker;
import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.RDFNode;

public class OFPSelectTester {

    static Linker testLinker = new OFPLinker();

    void test() {

    }

    public static void printResults(ResultSet rs) {
        while (rs.hasNext()) {
            QuerySolution soln = rs.nextSolution();
            RDFNode x = soln.get("?company");       // Get a result variable by name.
            RDFNode y = soln.get("?label");
            System.out.printf("%s %s \n", x, y);
        }
    }

    public static void main(String args[]) {
        ParameterizedSparqlString pss = new ParameterizedSparqlString();
        pss.setCommandText("SELECT ?company ?label WHERE {?company rdf:type ofp:PublicCompany ." +
                "?company skos:prefLabel ?label} LIMIT 10");
        pss.setNsPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        pss.setNsPrefix("ofp", "http://www.openfilings.org/ofp/2021/5/");
        pss.setNsPrefix("skos", "http://www.w3.org/2004/02/skos/core#");
        pss.setNsPrefix("foaf", "http://xmlns.com/foaf/0.1/");
        ResultSet rs = testLinker.selectQuery(pss);
        printResults(rs);
    }

}
