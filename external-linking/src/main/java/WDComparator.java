import Linkers.DBPLinker;
import Linkers.OFPLinker;
import Linkers.WikiDataLinker;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.util.ResourceUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class WDComparator {

    private WikiDataLinker wdLinker = new WikiDataLinker();
    private OFPLinker ofpLinker = new OFPLinker();

    private String wdType;
    private String ofpType;
    private String wdLabel;
    private String ofpLabel;


    private int maxDistance;

    public void setTypes(String wdType, String ofpType) {
        this.wdType = wdType;
        this.ofpType = ofpType;
    }

    public void setLabels(String wdLabel, String ofpLabel) {
        this.wdLabel = wdLabel;
        this.ofpLabel = ofpLabel;
    }

    public void setMaxDistance(int maxDistance) {
        this.maxDistance = maxDistance;
    }



    public  List<List<String>> getMatches() {
        ResultSet rsWd = getWdResults();
        ResultSet rsOfp = getOfpResults();

        EditDistance distancer = new EditDistance();

        ResultSet rsWdCopy = ResultSetFactory.copyResults(rsWd);

        List<List<String>> returnme = new ArrayList<List<String>>();

        int counter = 0;
        while (rsOfp.hasNext()) {
            QuerySolution solnOfp = rsOfp.next();
            String ofpCompanyLabel = solnOfp.get("?ofpLabel").asLiteral().getString();
            String ofpCompanyUri = solnOfp.getResource("?ofpCompany").toString();

            rsWd = ResultSetFactory.copyResults(rsWdCopy);


            while (rsWd.hasNext()) {
                QuerySolution solnWd = rsWd.next();
                String wdCompanyLabel = solnWd.get("?wdLabel").asLiteral().getString();
                String wdCompanyUri = solnWd.getResource("?wdEntity").toString();

                if (distancer.calculate(preprocess(ofpCompanyLabel), preprocess(wdCompanyLabel)) < this.maxDistance) {
                    List<String> tmp = new ArrayList<String>();
                    tmp.add(ofpCompanyUri);
                    tmp.add(wdCompanyUri);
                    returnme.add(tmp);
                    System.out.printf("%d) MATCH -- %s (%s)-- WITH --  %s (%s) -- \n\n",counter, ofpCompanyLabel, ofpCompanyUri, wdCompanyLabel,wdCompanyUri);
                    counter++;
                }
            }
        }
        return returnme;
    }

    private ResultSet getWdResults() {
        ParameterizedSparqlString pss = new ParameterizedSparqlString();
        String queryString = ("SELECT ?wdEntity ?wdLabel WHERE {?wdEntity wdt:P31 "+ this.wdType + " . ?wdEntity " + this.wdLabel + " ?wdLabel}");
        pss.setCommandText(queryString);
        pss.setNsPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        pss.setNsPrefix("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
        pss.setNsPrefix("dbo", "http://dbpedia.org/ontology/");
        pss.setNsPrefix("foaf", "http://xmlns.com/foaf/0.1/");
        pss.setNsPrefix("ofp", "http://www.openfilings.org/ofp/2021/5/");
        pss.setNsPrefix("skos", "http://www.w3.org/2004/02/skos/core#");
        pss.setNsPrefix("wd", "http://www.wikidata.org/entity/");
        pss.setNsPrefix("wdt", "http://www.wikidata.org/prop/direct/");

        ResultSet rs = wdLinker.selectQuery(pss);
        System.out.println("Got wd results.");
        return rs;
    }

    private ResultSet getOfpResults() {
        System.out.println("Getting ofp results.");
        ParameterizedSparqlString pss = new ParameterizedSparqlString();
        String queryString = ("SELECT ?ofpCompany ?ofpLabel WHERE {?ofpCompany rdf:type "+ this.ofpType + ". ?ofpCompany "  + this.ofpLabel + " ?ofpLabel}");
        pss.setCommandText(queryString);
        pss.setNsPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        pss.setNsPrefix("dbp", "http://dbpedia.org/ontology/");
        pss.setNsPrefix("foaf", "http://xmlns.com/foaf/0.1/");
        pss.setNsPrefix("ofp", "http://www.openfilings.org/ofp/2021/5/");
        pss.setNsPrefix("skos", "http://www.w3.org/2004/02/skos/core#");
        ResultSet rs = ofpLinker.selectQuery(pss);
        System.out.println("Got ofp results.");
        return rs;
    }

    public String preprocess(String processme) {
        String str = processme.toLowerCase();
        str = StringUtils.substringBefore(processme, "Inc");
        str = StringUtils.substringBefore(str, "N.V.");
        str = StringUtils.substringBefore(str, "PLC");


        return str;
    }

}
