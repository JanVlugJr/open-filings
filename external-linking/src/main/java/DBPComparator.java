import Linkers.DBPLinker;
import Linkers.OFPLinker;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.util.ResourceUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class DBPComparator {

    private DBPLinker dbpLinker = new DBPLinker();
    private OFPLinker ofpLinker = new OFPLinker();

    private String dbpType;
    private String ofpType;
    private String dbpLabel;
    private String ofpLabel;


    private int maxDistance;

    public void setTypes(String dbpType, String ofpType) {
        this.dbpType = dbpType;
        this.ofpType = ofpType;
    }

    public void setLabels(String dbpLabel, String ofpLabel) {
        this.dbpLabel = dbpLabel;
        this.ofpLabel = ofpLabel;
    }

    public void setMaxDistance(int maxDistance) {
        this.maxDistance = maxDistance;
    }



    public  List<List<String>> getMatches() {
        ResultSet rsDbp = getDbpResults();
        ResultSet rsOfp = getOfpResults();

        EditDistance distancer = new EditDistance();

        ResultSet rsDbpCopy = ResultSetFactory.copyResults(rsDbp);

        List<List<String>> returnme = new ArrayList<List<String>>();

        int counter = 0;
        while (rsOfp.hasNext()) {
            QuerySolution solnOfp = rsOfp.next();
            String ofpCompanyLabel = solnOfp.get("?ofpLabel").asLiteral().getString();
            String ofpCompanyUri = solnOfp.getResource("?ofpCompany").toString();

            rsDbp = ResultSetFactory.copyResults(rsDbpCopy);


            while (rsDbp.hasNext()) {
                QuerySolution solnDbp = rsDbp.next();
                String dbpCompanyLabel = solnDbp.get("?dbpLabel").asLiteral().getString();
                String dbpCompanyUri = solnDbp.getResource("?dbpCompany").toString();

                if (distancer.calculate(preprocess(ofpCompanyLabel), preprocess(dbpCompanyLabel)) < this.maxDistance) {
                    List<String> tmp = new ArrayList<String>();
                    tmp.add(ofpCompanyUri);
                    tmp.add(dbpCompanyUri);
                    returnme.add(tmp);
                    System.out.printf("%d) MATCH -- %s (%s)-- WITH --  %s (%s) -- \n\n",counter, ofpCompanyLabel, ofpCompanyUri, dbpCompanyLabel,dbpCompanyUri);
                    counter++;
                }
            }
        }
        return returnme;
    }

    private ResultSet getDbpResults() {
        ParameterizedSparqlString pss = new ParameterizedSparqlString();
        String queryString = ("SELECT ?dbpCompany ?dbpLabel WHERE {?dbpCompany rdf:type "+ this.dbpType + " . ?dbpCompany " + this.dbpLabel + " ?dbpLabel}");
        pss.setCommandText(queryString);
        pss.setNsPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        pss.setNsPrefix("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
        pss.setNsPrefix("dbo", "http://dbpedia.org/ontology/");
        pss.setNsPrefix("foaf", "http://xmlns.com/foaf/0.1/");
        pss.setNsPrefix("ofp", "http://www.openfilings.org/ofp/2021/5/");
        pss.setNsPrefix("skos", "http://www.w3.org/2004/02/skos/core#");
        ResultSet rs = dbpLinker.selectQuery(pss);
        System.out.println("Got dbp results.");
        return rs;
    }

    private ResultSet getOfpResults() {
        System.out.println("Getting ofp results.");
        ParameterizedSparqlString pss = new ParameterizedSparqlString();
        String queryString = ("SELECT ?ofpCompany ?ofpLabel WHERE {?ofpCompany rdf:type "+ this.ofpType + ". ?ofpCompany "  + this.ofpLabel + " ?ofpLabel}");
        pss.setCommandText(queryString);
        pss.setNsPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        pss.setNsPrefix("dbp", "http://dbpedia.org/ontology/");
        pss.setNsPrefix("foaf", "http://xmlns.com/foaf/0.1/");
        pss.setNsPrefix("ofp", "http://www.openfilings.org/ofp/2021/5/");
        pss.setNsPrefix("skos", "http://www.w3.org/2004/02/skos/core#");
        ResultSet rs = ofpLinker.selectQuery(pss);
        System.out.println("Got ofp results.");
        return rs;
    }

    public String preprocess(String processme) {
        String str = processme.toLowerCase();
        str = StringUtils.substringBefore(processme, "Inc");
        str = StringUtils.substringBefore(str, "N.V.");
        str = StringUtils.substringBefore(str, "PLC");


        return str;
    }





}
