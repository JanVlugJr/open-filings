import Linkers.Linker;
import Linkers.OFPLinker;
import org.apache.jena.base.Sys;
import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Resource;

import java.util.List;

public class SameAsAdder {

    OFPLinker ofpLinker = new OFPLinker();

    public int addSameAs( List<List<String>> results) {
        int counter = 0;

       for (List<String> entities : results) {
           String ofpEntity = entities.get(0);
           String otherEntity = entities.get(1);
           if (!exists(ofpEntity, otherEntity)) {
               ParameterizedSparqlString pss = new ParameterizedSparqlString();
               String queryString = ("insert data { <"+ ofpEntity +"> owl:sameAs <"+ otherEntity +"> }");
               pss.setCommandText(queryString);
               pss.setNsPrefix("ofp", "http://www.openfilings.org/ofp/2021/5/");
               pss.setNsPrefix("owl", "http://www.w3.org/2002/07/owl#");
               ofpLinker.addSameAs(ofpEntity, otherEntity );
               System.out.printf("Added match between %s and %s. \n", ofpEntity, otherEntity);
               counter++;
           }

       }


        return counter;
    }

    public boolean exists(String ofpEntity, String otherEntity) {
        ParameterizedSparqlString pss = new ParameterizedSparqlString();
        String queryString = ("select ?entity where { <"+ ofpEntity +"> owl:sameAs ?entity }");
        pss.setCommandText(queryString);
        pss.setNsPrefix("ofp", "http://www.openfilings.org/ofp/2021/5/");
        pss.setNsPrefix("owl", "http://www.w3.org/2002/07/owl#");
        ResultSet rs = ofpLinker.selectQuery(pss);


        while (rs.hasNext()) {
            QuerySolution qs = rs.next();
            Resource entity = qs.getResource("?entity");
            System.out.println("Checking equality.");
            if (entity.toString().equals(otherEntity)) {
                System.out.println("Already matched.");
                return true;
            }
        }
        //ofpLinker.closeConnection();
        System.out.println("Not yet matched.");
        return false;
    }

    private void newOfpLinker() {
        this.ofpLinker = new OFPLinker();
    }


}
