import java.util.List;

public class Main {



    public static void main(String args[]) {
//        DBPComparator dbpComparator = new DBPComparator();
//
        SameAsAdder adder = new SameAsAdder();
//
//        dbpComparator.setTypes("dbo:Company", "ofp:PublicCompany");
//        dbpComparator.setLabels("rdfs:label", "skos:prefLabel");
//        dbpComparator.setMaxDistance(2);
//        List<List<String>> results = dbpComparator.getMatches();
//
//       int added = adder.addSameAs(results);
//
//       System.out.printf("New added: %d \n", added);
//
//        DBPComparator dbpComparator2 = new DBPComparator();
//
//        dbpComparator2.setTypes("foaf:Person", "foaf:Person");
//        dbpComparator2.setLabels("foaf:name", "foaf:name");
//        dbpComparator2.setMaxDistance(2);
//        results = dbpComparator2.getMatches();
//
//
//        added = adder.addSameAs(results);
//
//        System.out.printf("New added: %d \n", added);
//
//
//        WDComparator wdComparator = new WDComparator();
//
//        wdComparator.setTypes("wd:Q891723", "ofp:PublicCompany");
//        wdComparator.setLabels("rdfs:label", "skos:prefLabel");
//        wdComparator.setMaxDistance(2);
 //       results = wdComparator.getMatches();
//
//        added = adder.addSameAs(results);
//
//        System.out.printf("New company links added: %d \n", added);
//
       WDComparator wdComparator2 = new WDComparator();


        wdComparator2.setTypes("wd:Q5", "foaf:Person");
        wdComparator2.setLabels("rdfs:label", "foaf:name");
        wdComparator2.setMaxDistance(2);
        List<List<String>> results = wdComparator2.getMatches();

        int added = adder.addSameAs(results);

        System.out.printf("New person links added: %d \n", added);

    }
}
