package Linkers;

import Linkers.Linker;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionFuseki;
import org.apache.jena.rdfconnection.RDFConnectionRemoteBuilder;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.OWL;

// https://github.com/apache/jena/blob/main/jena-rdfconnection/src/main/java/org/apache/jena/rdfconnection/examples/RDFConnectionExample4.java
// https://github.com/apache/jena/blob/main/jena-rdfconnection/src/main/java/org/apache/jena/rdfconnection/examples/RDFConnectionExample6.java

public class OFPLinker implements Linker {

    RDFConnectionRemoteBuilder builder = RDFConnectionFuseki.create()
            .destination("http://localhost:3030/ofplinked/sparql");

    RDFConnectionFuseki ofpConn = (RDFConnectionFuseki)builder.build();

    RDFConnectionRemoteBuilder updateBuilder = RDFConnectionFuseki.create()
            .destination("http://localhost:3030/ofplinked/update");

    RDFConnectionFuseki ofpUpdateConn = (RDFConnectionFuseki)updateBuilder.build();

    private ResultSet rs;

    Model model;

    public OFPLinker() {
        Dataset dataset = RDFDataMgr.loadDataset("/Users/jan/Documents/ComputerScience/scriptie/externalEntityLinking/kb.ttl");
        this.model = dataset.getDefaultModel();
        model.setNsPrefix("owl", OWL.getURI());
    }





    @Override
    public ResultSet selectQuery(ParameterizedSparqlString pss) {
        System.out.println(pss.toString());
        Query query = QueryFactory.create(pss.toString());
        try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {
            ResultSet results = qexec.execSelect();
            ResultSet rs = ResultSetFactory.copyResults(results) ;
            return rs ;
        }
    }


    public void insertQuery(ParameterizedSparqlString pss) {

        return;
//        try ( RDFConnection conn = this.ofpUpdateConn ) {
//            System.out.println(pss.toString());
//            conn.update(pss.toString());
//        }

    }

    public void addSameAs(String subject, String object) {
        Resource subjResource = model.getResource(subject);
        Resource objResource = ResourceFactory.createResource(object);
        Property p = model.getProperty("owl:sameAs");
        subjResource.addProperty(p, object);

    }

    public void resetConnection() {
        ofpConn.close();

        this.ofpConn = (RDFConnectionFuseki)builder.build();

        this.ofpUpdateConn = (RDFConnectionFuseki)updateBuilder.build();


    }

}
