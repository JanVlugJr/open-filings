package Linkers;

import org.apache.jena.query.*;


public interface Linker {

    public ResultSet selectQuery(ParameterizedSparqlString pss);

}
