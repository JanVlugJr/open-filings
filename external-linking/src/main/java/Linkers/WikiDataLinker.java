package Linkers;

import Linkers.Linker;
import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFactory;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionRemote;

public class WikiDataLinker implements Linker {

    RDFConnection wdConn = RDFConnectionRemote.create()
            .destination("https://query.wikidata.org/")
            .queryEndpoint("sparql")
            .build();



    @Override
    public ResultSet selectQuery(ParameterizedSparqlString pss) {
        try ( RDFConnection conn = wdConn ) {
            System.out.println(pss.toString());
            ResultSet rs = conn.query(pss.toString()).execSelect();
            return ResultSetFactory.copyResults(rs) ;
        }
    }
}