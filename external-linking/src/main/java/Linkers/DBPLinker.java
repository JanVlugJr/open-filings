package Linkers;

import Linkers.Linker;
import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionRemote;

public class DBPLinker implements Linker {

    RDFConnection dbpConn = RDFConnectionRemote.create()
            .destination("http://dbpedia.org/")
            .queryEndpoint("sparql")
            .build();




    @Override
    public ResultSet selectQuery(ParameterizedSparqlString pss) {
        try ( RDFConnection conn = dbpConn ) {
            System.out.println(pss.toString());
            ResultSet rs;
            rs = conn.query(pss.toString()).execSelect();
            return ResultSetFactory.copyResults(rs) ;
        }
    }

    public void closeConnection() {
        dbpConn.close();
    }
}