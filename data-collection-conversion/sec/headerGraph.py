
from rdflib import Graph, URIRef, BNode, Literal, Namespace, CSVW, DC, DCAT, DCTERMS, DOAP, FOAF, ODRL2, ORG, OWL, \
                           PROF, PROV, RDF, RDFS, SDO, SH, SKOS, SOSA, SSN, TIME, \
                           VOID, XMLNS, XSD

#Setting up graph and defining namespaces
g = Graph()
g.bind("dc", DC)
baseNs = Namespace("urn:ofp:")
filingNs = Namespace("urn:ofp:filing:")
reguNs = Namespace("urn:ofp:regulator:")
companyNs = Namespace("urn:ofp:company:")

#Defining classes and properties of the namespace
Class = RDFS.Class
filingObject = URIRef('urn:ofp:filing')
g.add((filingObject, RDF.type, Class))

ownershipFilingObject = URIRef('urn:ofp:ownershipfiling')
g.add( (ownershipFilingObject, RDF.type, Class))
g.add((ownershipFilingObject, RDFS.subClassOf, filingObject))
g.add((ownershipFilingObject, SKOS.prefLabel, Literal("OWNERSHIP FILING")))

#cikProperty = URIRef(baseNs + 'hasCik') # TO BE REPLACED BY NOTATION
#g.add(cikProperty, RDF.type, RDF.Property)

rptOwnerProperty = URIRef(baseNs + 'hasRptOwner')
g.add((rptOwnerProperty, RDF.type, RDF.Property))

pricePerInstrument = URIRef(baseNs + 'hasPricePer')
g.add((rptOwnerProperty, RDF.type, RDF.Property))