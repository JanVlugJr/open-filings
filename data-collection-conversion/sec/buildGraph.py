import headerGraph as ns
from rdflib import Graph, URIRef, BNode, Literal, Namespace, CSVW, DC, DCAT, DCTERMS, DOAP, FOAF, ODRL2, ORG, OWL, \
                           PROF, PROV, RDF, RDFS, SDO, SH, SKOS, SOSA, SSN, TIME, \
                           VOID, XMLNS, XSD
from bs4 import BeautifulSoup
import uuid


def createIssuerTriples(cik, name):
    issuer = URIRef(ns.companyNs +  str(uuid.uuid4()))
    ns.g.add((issuer, RDF.type, ORG.Organization))
    ns.g.add((issuer, SKOS.prefLabel, Literal(name)))
    print(ns.g.serialize(format="turtle").decode("utf-8"))



def addAllIssuers(soup):
    issuers = soup.findAll('issuer')
    for issuer in issuers:
        cik = issuer.findChild('issuercik')
        name = issuer.findChild('issuername')
        print('found a issuer with name ' + name.text)
        createIssuerTriples(cik.text, name.text)


def createOwnerShipFilingFilingWithMetaData(regulator, filingnumber):
    filing = URIRef(ns.filingNs + str(filingnumber))
    ns.g.add((filing, RDFS.type, ns.ownershipFilingObject))


    return filing;

