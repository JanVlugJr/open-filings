from bs4 import BeautifulSoup
import pandas as pd
import numpy as np
from html_table_extractor.extractor import Extractor


class HtmlConverter:
    def __init__(self, path):
        self.path = path

    def convertHtml(self):
        print('Starting filing conversion')
        with open(self.path) as f:
            doc = f.read()
        soup = BeautifulSoup(doc, "html.parser")
        table = soup.findAll('table')

        self.getDfFromTable(table)

        print(self.df1.head())
        print(self.df2.head())

    def getDfFromTable(self, table):
        df = pd.read_html(str(table))[0]


        # take top non variable part of df
        topdf = df.iloc[:12, :]
       # topdf = topdf.drop(df.index[[8]])
        topdf = topdf.T

        topdf.iloc[0][1] = 'OwnerName'
        topdf.columns = topdf.iloc[0]

        topdf.drop(index=topdf.index[0], axis=0, inplace=True)

        nan_value = float("NaN")
        topdf.replace("", nan_value, inplace=True)
        topdf.dropna(how='all', axis=1, inplace=True)


        topdf = topdf.rename(columns={"Category":"Pet"})
        self.df1 = topdf


        # take var part of filing
        rows = df.shape[0]
        varrows = df.shape[0] - 23
        transactions = df[12: (12 + varrows)]
        transactions.columns = ['PricePerShare', 'TotalValue']

        # take bottom non variable part of filing
        botdf = df[(12 + varrows):(df.shape[0]- varrows)]
        botdf = botdf.T
        botdf.columns = botdf.iloc[0]

        botdf.drop(index=botdf.index[0], axis=0, inplace=True)

        nan_value = float("NaN")
        botdf.replace("", nan_value, inplace=True)
        botdf.dropna(how='all', axis=1, inplace=True)
        self.df2 = botdf

        return




    def getdf1(self):
        return self.df1

    def getdf2(self):
        return self.df2
    # def mergeDfs(self, top, bot, transactions):
    #
    #     totalcolumns = top.columns.values.tolist() + bot.columns.values.tolist()
    #
    #
    #     #merge
    #     for i in range(0, bot.shape[1]):
    #         value = bot.iloc[0][i]
    #         top[str(i + top.shape[1])] = value
    #
    #     print(top.head())
    #     return
    #
    #     top.columns = totalcolumns
    #
    #     top = pd.DataFrame(np.repeat(top.values, (transactions.shape[0]), axis=0))
    #
    #     top.to_csv('top.csv', header=True, index=False)
    #     print('filing')
    #     PricePerShare = []
    #     TotalValue = []
    #     for i in range(0,transactions.shape[0]):
    #         PricePerShare.append(transactions.iloc[i]['PricePerShare'])
    #         TotalValue.append(transactions.iloc[i]['TotalValue'])
    #
    #     top['PricePerShareTrans'] = PricePerShare
    #     top['TotalValueTrans'] = TotalValue
    #
    #     top.to_csv('top.csv', header=True, index=False)
    #     nan_value = float("NaN")
    #     top.replace("", nan_value, inplace=True)
    #     top.dropna(how='all', axis=1, inplace=True)
    #
    #     print(top.head())
    #
    #     #self.giveTitles(top)
    #
    #     print(top.head())
    #
    #     top.to_csv('top.csv', header=True, index=False)
    #
    # def giveTitles(self, df):
    #     columnNames = ['FilingDescription', 'RptOwner', 'Role', 'InitialNotAmendment', 'Issuer', 'Adress', 'Country', 'LEI', 'Instrument', 'ISIN', 'TransDescription', 'TotalFilingValue', 'PricePerShare', 'Date', 'TransLoc', 'WhereReported', 'ReportDate', 'Language', 'PricePerShare', 'TotalValue']
    #     df.columns = columnNames



  #
#
# 0                                            Subject:  Notification and public disclosure of  transac...
# 1                                               Name:                                      Karl von Rohr
# 2                                a)  Position/status:                        Member of the managing body
# 3                b) Initial  notification/ Amendment:                               Initial notification
# 4                                               Name:                                   Deutsche Bank AG
# 5                                            Address:             Taunusanlage 12, 60325 Frankfurt a. M.
# 6                                            Country:                                        Deutschland
# 7                                                LEI:                               7LTWFZYICNSX8D621K86
# 8        a) Description of the  financial instrument:       a) Description of the  financial instrument:
# 9                                Type of  instrument:                                              Share
# 10                              Identification  code:                                       DE0005140008
# 11                     b) Nature of the  transaction:  Delivery of shares resulting from compensation...
# 12                                        10.3140 EUR                                    131761.3500 EUR
# 13                                 Aggregated volume:                                    131761.3500 EUR
# 14                                             Price:                                        10.3140 EUR
# 15                       e) Date of the  transaction:                                         10.03.2021
# 16                      f) Place of the  transaction:                            Outside a trading venue
# 17                              Further  information:                                                NaN
# 18                  Obligatory publication  in/about:  DGAP/EQS Group AG, veröffentlicht über  Thomso...
# 19                        Obligatory publication  on:                                         12.03.2021
# 20                         Language of the  document:                                            English
# 21                                                NaN                                                NaN
# 22  Subject: Notification and public disclosure of...                                                NaN
# 23                                                NaN                                                NaN
# BREAAAAAAK



