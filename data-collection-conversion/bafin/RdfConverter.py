import datetime

from bs4 import BeautifulSoup
import pandas as pd
import numpy as np
from html_table_extractor.extractor import Extractor
from bs4 import BeautifulSoup
from rdflib import Graph, URIRef, BNode, Literal, Namespace, CSVW, DC, DCAT, DCTERMS, DOAP, FOAF, ODRL2, ORG, OWL, \
                           PROF, PROV, RDF, RDFS, SDO, SH, SKOS, SOSA, SSN, TIME, \
                           VOID, XMLNS, XSD
import os
import requests
import time
import uuid


OFP = Namespace("http://www.openfilings.org/ofp/2021/5/")
LOCN = Namespace("http://www.w3.org/ns/locn#")
ns = Namespace("http://www.openfilings.org/")
OwnershipFiling = URIRef("http://www.openfilings.org/ofp/2021/5/OwnershipFiling")
Regulator = URIRef("http://www.openfilings.org/ofp/2021/5/Regulator")



class RdfConverter():
    def __init__(self, df1, df2, url, pdfurl, id):
        self.df1 = df1
        self.df2 = df2
        self.url = url
        self.pdfurl = pdfurl
        self.id = id
        self.issuerLei = None
        self.issuerAdress = None
        self.volume = None
        self.pricepershare = None


    def convert(self):
        self.name = None
        self.position = None
        self.adress = None
        self.filingUri = URIRef(self.url)
        self.initGraph()
        self.generateBase()
        for (index, series) in self.df1.iterrows():
            for (column, value) in series.iteritems():
                # Language
                if (column == 'Language of the document:' or column == 'Sprachen der Veröffentlichung:'):
                    if (value == 'English'):
                        self.g.add((self.filingUri, DC.language, Literal('en')))
                        self.language = 'en'
                    elif (value == 'Deutsch'):
                        self.g.add((self.filingUri, DC.language, Literal('de')))
                        self.language = 'de'

                # Date
                if (column == 'Pflichtveröffentlichung am:' or column == 'Obligatory publication on:' or column == 'Datum:'):
                    d = datetime.datetime.strptime(value, '%d.%m.%Y')
                    dateLiteral = Literal(d.strftime('%Y-%m-%d'), datatype=XSD.date)
                    self.g.add((self.filingUri, DC.date, dateLiteral))

                # Rpt Owner Name
                if (column == 'OwnerName'):
                    self.name = value

                # Rpt Owner Position
                if (column == 'a)  Position/Status:'  or column == 'Berufliche Rolle::'):
                    self.position = value

                if (column == 'Name:'):
                    self.issuername = value

                if (column == 'Adress:' or column == 'Adresse:'):
                    self.issuerAdress = value

                if (column == 'Staat:' or column == 'Country:'):
                    self.issuerCountry = value

                if (column == 'LEI:'):
                    self.issuerLei = value

                if (column == 'Type of  instrument:' or column == 'Art des  Instruments:' or column == 'Bezeichnung:'):
                    self.instrumentLabel = value

                if (column == 'Kennung:' or column == 'Identification  code:' or column == 'ISIN:'):
                    self.instrumentIsin = value

                if (column == 'b) Nature of the  transaction:' or column == 'b) Art des  Geschäfts:' or column == 'Geschäftsart:'):
                    self.transactionDescription = value

                if (column == 'Aggregated volume:' or column == 'Aggregiertes Volumen:' or column == 'Geschäftsvolumen:'):
                    self.volume = value

                if (column == 'Price:' or column == 'Preis:'):
                    self.pricepershare = value

                if (column == 'Aggregated volume:' or column == 'Aggregiertes Volumen:'):
                    self.volume = value

                if (column == 'e) Date of the  transaction:' or column == 'e) Datum des Geschäfts:' or column == 'Datum:'):
                    d = datetime.datetime.strptime(value, '%d.%m.%Y')
                    self.transdate = d.strftime('%Y-%m-%d')

        for (index, series) in self.df2.iterrows():
            for (column, value) in series.iteritems():
                # Language
                if (column == 'Language of the document:' or column == 'Sprachen der Veröffentlichung:'):
                    if (value == 'English'):
                        self.g.add((self.filingUri, DC.language, Literal('en')))
                        self.language = 'en'
                    elif (value == 'Deutsch'):
                        self.g.add((self.filingUri, DC.language, Literal('de')))
                        self.language = 'de'

                # Date
                if (column == 'Pflichtveröffentlichung am:' or column == 'Obligatory publication on:'):
                    d = datetime.datetime.strptime(value, '%d.%m.%Y')
                    dateLiteral = Literal(d.strftime('%Y-%m-%d'), datatype=XSD.date)
                    self.g.add((self.filingUri, DC.date, dateLiteral))

                # Rpt Owner Name
                if (column == 'OwnerName'):
                    self.name = value

                # Rpt Owner Position
                if (column == 'a)  Position/Status:'):
                    self.position = value

                if (column == 'Name:'):
                    self.issuername = value

                if (column == 'Adress:' or column == 'Adresse:'):
                    self.issuerAdress = value

                if (column == 'Staat:' or column == 'Country:'):
                    self.issuerCountry = value

                if (column == 'LEI:'):
                    self.issuerLei = value

                if (column == 'Type of  instrument:' or column == 'Art des Instruments:' or column == 'Bezeichnung:'):
                    self.instrumentLabel = value

                if (column == 'Kennung:' or column == 'Identification code:' or column == 'ISIN:'):
                    self.instrumentIsin = value

                if (column == 'b) Nature of the  transaction:' or column == 'b) Art des Geschäfts:' or column == 'Geschäftsart:'):
                    self.transactionDescription = value

                if (column == 'Aggregated  volume:' or column == 'Aggregiertes  Volumen:' or column == 'Geschäftsvolumen:'):
                    self.volume = value

                if (column == 'Price:' or column == 'Preis:'):
                    self.pricepershare = value

                if (column == 'Aggregated volume:' or column == 'Aggregiertes Volumen:'):
                    self.volume = value

                if (column == 'e) Date of the  transaction:' or column == 'e) Datum des  Geschäfts:' or column == 'Datum:'):
                    d = datetime.datetime.strptime(value, '%d.%m.%Y')
                    self.transdate = d.strftime('%Y-%m-%d')

        self.generateIssuer()
        self.generateOwner()
        self.generateInstrument()
        self.generateTransaction()


    def initGraph(self):
        self.g = Graph()
        self.g.bind("skos", SKOS)
        self.g.bind("foaf", FOAF)
        self.g.bind("dc", DC)
        self.g.bind("org", ORG)
        self.g.bind("ofp", OFP)
        self.g.bind("locn", LOCN)

    def generateBase(self):
        # Filing and metadata
        self.g.add((self.filingUri, RDF.type, OwnershipFiling))
        self.g.add((self.filingUri, SKOS.prefLabel, Literal("Ownership filing")))
        self.g.add((self.filingUri, SKOS.altLabel, Literal(str(self.id))))

        pdfUri = URIRef(self.pdfurl)
        self.g.add((pdfUri, SKOS.prefLabel, Literal('PDF version of ownership filing ' + str(self.id))))
        self.g.add((pdfUri, DC.Format, Literal("PDF")))

        self.g.add((self.filingUri, DC.hasVersion, pdfUri))
        self.g.add((pdfUri, DC.versionOf, self.filingUri))

        # Publisher
        UntReg = URIRef(ns + 'regulators/untreg')
        self.g.add((UntReg, SKOS.prefLabel, Literal("Unternehmensregister")))
        self.g.add((UntReg, SKOS.prefLabel, Literal("Unternehmensregister")))
        self.g.add((UntReg, RDF.type, Regulator))
        self.g.add((self.filingUri, DC.publisher, UntReg))

        # Filing type
        self.g.add((self.filingUri, DC.type, URIRef(ns + '/untregfilingtypes/' + 'transactiondisclosure')))
        self.g.add((URIRef(ns + '/untregfilingtypes/' + 'transactiondisclosure'), SKOS.prefLabel, Literal("Notification and public disclosure of transactions by persons discharging managerial responsibilities and persons closely associated with them")))

    def generateOwner(self):
        ownerUri = URIRef(ns + 'people/' + str(uuid.uuid4()))
        self.rptOwner = ownerUri
        self.g.add((ownerUri, RDF.type, FOAF.Person))
        if(self.name):
            self.g.add((ownerUri, FOAF.name, Literal(self.name)))

        self.generateMembership(ownerUri)

        self.g.add((self.filingUri, OFP.hasRptOwner, ownerUri))

    def generateMembership(self, ownerUri):
        membership = URIRef(ns + 'memberships/' + str(uuid.uuid4()))

        self.g.add((membership, ORG.organization, self.issuer))

        self.g.add((membership, ORG.member, ownerUri))
        self.g.add((ownerUri, ORG.hasMember, membership))

        role = URIRef(ns + 'roles/' + str(uuid.uuid4()))
        self.g.add((role, RDF.type, OFP.Role))
        self.g.add((role, SKOS.prefLabel, Literal(self.position)))
        self.g.add((membership, ORG.role, role))

        self.g.add((membership, RDF.type, ORG.Membership))

    def generateIssuer(self):
        issuerUri = URIRef(ns + 'companies/' + str(uuid.uuid4()))
        self.issuer = issuerUri

        if (self.issuername):
            print('found a issuer with name ' + self.issuername)
            self.g.add((issuerUri, SKOS.prefLabel, Literal(self.issuername)))
            # Change preflabel of filing to include issuer name
            self.g.set((self.filingUri, SKOS.prefLabel, Literal("Ownership filing of " + self.issuername)))


        if (self.issuerLei):
            self.g.add((issuerUri, OFP.haslei, Literal(self.issuerLei, datatype=OFP.lei)))

        if (self.issuerAdress):
            adressObject = URIRef(ns + 'adresses/' + str(uuid.uuid4()))
            self.g.add((adressObject, RDF.type, URIRef('http://www.w3.org/ns/locn#Address')))
            self.g.add((adressObject, LOCN.fullAddress, Literal(self.issuerAdress)))
            self.g.add((issuerUri, LOCN.adress, adressObject))

        self.g.add((issuerUri, RDF.type, OFP.PublicCompany))
        self.g.add((self.filingUri, OFP.issuedBy, issuerUri))

    def generateInstrument(self):
        self.instrument = URIRef(ns + 'instruments/' + str(uuid.uuid4()))
        self.g.add((self.instrument, RDF.type, OFP.Instrument))
        self.g.add((self.instrument, SKOS.prefLabel, Literal(self.instrumentLabel, datatype=XSD.string)))
        self.g.add((self.instrument, OFP.hasisin, Literal(self.instrumentIsin, datatype=OFP.isin)))
        self.g.add((self.instrument, OFP.underlyingCompany, self.issuer))

    def generateTransaction(self):
        self.transObj = URIRef(ns + 'transactions/' + str(uuid.uuid4()))
        self.g.add((self.transObj, RDF.type, OFP.Transaction))
        self.g.add((self.transObj, OFP.instrument, self.instrument))
        self.g.add((self.transObj, SKOS.prefLabel, Literal(self.transactionDescription, datatype=XSD.string)))

        # Add transdate
        self.g.add((self.transObj, OFP.transactionDate, Literal(self.transdate, datatype=XSD.date)))

        #volume
        if (type(self.volume) is str and self.volume != ''):
            self.g.add(
                (self.transObj, OFP.amountOfInstruments, Literal(self.volume, datatype=XSD.string)))
        else:
            self.g.add(
                (self.transObj, OFP.amountOfInstruments, Literal(self.volume, datatype=XSD.decimal)))

        #price
        if (type(self.pricepershare) is str and self.pricepershare != ''):
            self.g.add(
                (self.transObj, OFP.pricePerInstrument, Literal(self.pricepershare, datatype=XSD.string)))
        else:
            self.g.add(
                (self.transObj, OFP.pricePerInstrument, Literal(self.pricepershare, datatype=XSD.decimal)))
            self.g.add((self.transObj, OFP.currency, Literal('EUR', datatype=OFP.currency)))


        self.generateOwnership()

        self.g.add((self.filingUri, OFP.reportsTransaction, self.transObj))

    def generateOwnership(self):

        tmpid = str(uuid.uuid4())
        self.ownership = URIRef(ns + 'ownerships/' + tmpid)
        self.g.add((self.ownership, SKOS.prefLabel, Literal('Ownership' + tmpid)))
        self.g.add((self.ownership, RDF.type, OFP.Ownership))
        self.g.add((self.rptOwner, OFP.owns, self.ownership))
        self.g.add((self.ownership, OFP.instrument, self.instrument))

    def saveGraph(self, path, rdfpath):
        print("saved RDF of str(self.id)")

        with open(rdfpath + str(self.id) + ".ttl", 'w') as f:
            f.write(self.g.serialize(format='turtle').decode("utf-8"))

        with open(path + ("rdf" + str(self.id) + ".txt"), 'w') as f:
            f.write(self.g.serialize(format='turtle').decode("utf-8"))
        return


