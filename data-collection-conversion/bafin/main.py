import os
import uuid

import requests

from HtmlConverter import *
from RdfConverter import *
from Filing import *

fn = None
sessionId = '339845FE2E362D427A84327EFA048293.web04-1'
baseUrl = 'https://www.unternehmensregister.de/ureg/result.html;jsessionid=' + sessionId

outputdir = '/Users/jan/Documents/ComputerScience/scriptie/BafinHtmlToRdf/output/scrapingoutput/'
rdfoutputdir = '/Users/jan/Documents/ComputerScience/scriptie/BafinHtmlToRdf/output/rdfoutput/'



def savePage(link):
    r = requests.get(link)
    soup = BeautifulSoup(r.text, "html.parser")
    divs = soup.find_all('div', {'class': "label_result"})
    for div in divs:
        element = div.find("span").find("a")
        if (element):
            print('Found new url.. making filing')
            url = baseUrl + element.get('href')
            filing = Filing(url, outputdir)
            filing.saveHTML()
            filing.savePDF()
            convert(filing)


def scrape(link):
    r = requests.get(link)
    soup = BeautifulSoup(r.text, "html.parser")

    next = soup.find("div", {'class': "next"}).findChild("a", {'class': "page-nav"})

    print('main page link = ' + link)
    savePage(link)

    while next.get('href'):
        link = 'https://www.unternehmensregister.de/ureg/result.html;jsessionid=' + sessionId + next.get('href')
        print(link)
        savePage(link)
        r = requests.get(link)
        soup = BeautifulSoup(r.text, "html.parser")
        next = soup.find("div", {'class': "next"}).findChild("a", {'class': "page-nav"})



def convert(filing):
    print('Converting filing ' + filing.id)
    file = filing.htmlloc
    converter = HtmlConverter(file)
    converter.convertHtml()
    df1 = converter.getdf1()
    df2 = converter.getdf2()
    df1.to_csv('df1.csv', header=True, index=False)
    df2.to_csv('df2.csv', header=True, index=False)
    print(df2.shape[1])
    if(df2.shape[1] < 3):
        return
    generator = RdfConverter(df1, df2, filing.htmlurl, filing.pdfurl, filing.id)
    generator.convert()
    generator.saveGraph(outputdir + filing.id + '/', rdfoutputdir )

def main():
    link = 'https://www.unternehmensregister.de/ureg/result.html;jsessionid=339845FE2E362D427A84327EFA048293.web04-1?submitaction=pathnav&page.1=page'
    scrape(link)



if __name__ == "__main__":
    main()