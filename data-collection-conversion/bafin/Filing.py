import requests
from bs4 import BeautifulSoup

from main import *


class Filing():
    def __init__(self, url, basedir):
        self.url = url
        self.id = str(uuid.uuid4())
        self.basedir = basedir
        self.outputdir = self.makeDir()

    def makeDir(self):
        os.mkdir(self.basedir + str(self.id))
        return self.basedir + str(self.id) + '/'

    def saveHTML(self):
        r = requests.get(self.url)
        print(self.url)
        soup = BeautifulSoup(r.text, "html.parser")
        element = soup.find('a', {'aria-label': "Print version"})
        self.htmlurl = 'https://www.unternehmensregister.de/ureg/result.html;jsessionid=' + sessionId + element.get('href')

        response = requests.get(self.htmlurl)
        filename = self.id + '.html'
        self.htmlloc = self.outputdir + filename
        with open(self.outputdir + filename, 'wb') as f:
            f.write(response.content)
        print('Saved html to ' + self.htmlloc)

    def savePDF(self):
        r = requests.get(self.url)
        soup = BeautifulSoup(r.text, "html.parser")
        element = soup.find('a', {'aria-label': "PDF format"})
        self.pdfurl = 'https://www.unternehmensregister.de/ureg/' + element.get('href')
        response = requests.get(self.pdfurl)
        filename = self.id + '.pdf'
        with open(self.outputdir + filename, 'wb') as f:
            f.write(response.content)

        print('Saved pdf to ' + self.outputdir + filename)

