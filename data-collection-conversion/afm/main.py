import uuid

import requests
from bs4 import BeautifulSoup
import time
from datetime import *
import pandas as pd
import os

from RdfGenerator import *

currId = None


def cleanHtml(htmlFile):
    with open(htmlFile) as f:
      table_doc = f.read()
    soup = BeautifulSoup(table_doc)
    mobileTitles = soup.find_all('span', {'class': "mobile-title"})
    for i in mobileTitles:
        i.decompose()
    return soup

def swapCommasPeriods(soup):
    table = soup.find('table', {'class': "results-details-register"})
    tablestr = str(table)
    tablestr = tablestr.replace(".", "")
    tablestr = tablestr.replace(",", ".")
    return tablestr



def getDfFirstTable(tablestr):
    tables = pd.read_html(tablestr)
    df1 = tables[0]
    return df1

def getIdFromUrl(url):
    index = url.find('id=')
    if(index != -1):
        id = url[index + 3:]
        return id
    else:
        return 'N/A'


def addMetaData(df, url):
    column = []
    for i in range(df.shape[0]):
        column.append(url)
    df['url'] = column

    column = []
    for i in range(df.shape[0]):
        column.append('AFM')

    df['Regulator'] = column

    column = []
    id = getIdFromUrl(url)
    for i in range(df.shape[0]):
        column.append(id)

    df['AFMid'] = column

    column = []
    for i in range(df.shape[0]):
        column.append('NL')

    df['Country Filed'] = column

    return df

def appendListToDf(df, soup, url):
    list = soup.find_all('ul', {'class': "overview"})
    liElements = list[0].findChildren('li')
    for li in liElements:
        column = []
        firstLi = li.findChild('span')
        columnName = firstLi.text
        #print(columnName)
        value = firstLi.find_next_sibling('span')
        print(value)
        for i in range(df.shape[0]):
            column.append(value.text)
        #print(column)
        df[columnName] = column

    df = addMetaData(df, url)
    return df

#obsolete
def saveCSV(url, path):
    soup = cleanHtml(path)
    table = swapCommasPeriods(soup)
    df = getDfFirstTable(soup)
    df = appendListToDf(df, soup, url)

    csvFileName = str(currId) + 'table' + 'Afm' + '.csv'

    df.to_csv('/Users/jan/Documents/ComputerScience/scriptie/CSVoutput/' + csvFileName, index=False)

    print('saved ' + csvFileName)

def convertDf(url, path):
    soup = cleanHtml(path)
    tablestr = swapCommasPeriods(soup)
    df = getDfFirstTable(tablestr)
    df = appendListToDf(df, soup, url)


    df.to_csv('/Users/jan/Documents/ComputerScience/scriptie/AFMHtmlToRdf/' + 'df.csv', index=False)


    dir = '/Users/jan/Documents/ComputerScience/scriptie/AFMHtmlToRdf/output/run1107/files/' + str(currId) + '/'
    rdfdir = '/Users/jan/Documents/ComputerScience/scriptie/AFMHtmlToRdf/output/run1107/rdf/'
    converter = RdfGenerator(currId, df, url)
    converter.convert()
    converter.saveGraph(dir, rdfdir)



def saveHTML(url):
    global currId
    response = requests.get(url)
    filename = str(currId) + '.html'
    os.mkdir('/Users/jan/Documents/ComputerScience/scriptie/AFMHtmlToRdf/output/run1107/files/' + str(currId))
    with open('/Users/jan/Documents/ComputerScience/scriptie/AFMHtmlToRdf/output/run1107/files/' + str(currId) + '/'+ filename, 'wb') as f:
        f.write(response.content)
    print('saved: ' + filename)
    path ='/Users/jan/Documents/ComputerScience/scriptie/AFMHtmlToRdf/output/run1107/files/' + str(currId) + '/'+ filename
    print(url)
    convertDf(url, path)




def savePage(url):
    global currId
    r = requests.get(url)
    soup = BeautifulSoup(r.text, "html.parser")

    spans = soup.find_all('span', {'class': "MIF"})

    if spans:
        print('found ' + str(len(spans)) + 'on this date')

    for span in spans:

        a = span.findChild('a')
        if a:
            currId = uuid.uuid4()
            url = 'https://www.afm.nl' + a.get('href')
            saveHTML(url)


link = 'https://www.afm.nl/nl-nl/professionals/registers/meldingenregisters/transacties-leidinggevenden-mar19-?KeyWords=&DateFrom=01-01-2020&DateTill=04-05-2021#results'

def scrape():
    for x in range(20000):
        # dateFrom = datetime.datetime.now() - timedelta(days=x)
        # dateTo = datetime.datetime.now() - timedelta(days=x)

        dateFrom = datetime.datetime(2019, 3, 18) - timedelta(days=x)
        dateTo = datetime.datetime(2019, 3, 18) - timedelta(days=x)

        dateFromString = dateFrom.strftime("%d") + '-' + dateFrom.strftime("%m") + '-' + dateFrom.strftime("%Y")
        dateToString = dateTo.strftime("%d") + '-' + dateTo.strftime("%m") + '-' + dateTo.strftime("%Y")

        print('saving day: ' + dateFromString)

        url = 'https://www.afm.nl/nl-nl/professionals/registers/meldingenregisters/transacties-leidinggevenden-mar19-?KeyWords=&DateFrom=' + dateFromString + '&DateTill=' + dateToString + '#results'

        savePage(url)



scrape()