import datetime

from bs4 import BeautifulSoup
import pandas as pd
import numpy as np
from html_table_extractor.extractor import Extractor
from bs4 import BeautifulSoup
from rdflib import Graph, URIRef, BNode, Literal, Namespace, CSVW, DC, DCAT, DCTERMS, DOAP, FOAF, ODRL2, ORG, OWL, \
                           PROF, PROV, RDF, RDFS, SDO, SH, SKOS, SOSA, SSN, TIME, \
                           VOID, XMLNS, XSD
import os
import requests
import time
from Transaction import *
import uuid

OFP = Namespace("http://www.openfilings.org/ofp/2021/5/")
LOCN = Namespace("http://www.w3.org/ns/locn#")
ns = Namespace("http://www.openfilings.org/")
OwnershipFiling = URIRef("http://www.openfilings.org/ofp/2021/5/OwnershipFiling")
Regulator = URIRef("http://www.openfilings.org/ofp/2021/5/Regulator")

class RdfGenerator():
    def __init__(self, currId, df, url):
        self.id = currId
        self.df = df
        self.url = url
        self.transactions = []
        self.ownerships = []
        self.lei = None
        self.date = None
        self.rptOwnerName = None
        self.issuerName = None
        self.lei = None
        self.position = None


    def convert(self):
        self.filingUri = URIRef(self.url)
        self.initGraph()
        self.generateBase()
        self.getTransactions()
        self.getGeneralAttributes()
        self.generateIssuer()
        self.generateOwner()
        self.generateTransactions()

    def getTransactions(self):



        for index, row in self.df.iterrows():
            currency, instrumentType, isin, transType, transDescr, derivative, price, amount = (None,) * 8
            if ('Type instrument' in self.df.columns):
                instrumentType =  row['Type instrument']
            if ('ISIN' in self.df.columns):
                isin =  row['ISIN']
            if ('Aard transactie' in self.df.columns):
                transType = row['Aard transactie']
            if ('Soort transactie' in self.df.columns):
                transDescr = row['Soort transactie']
            if ('Aandelenoptie programma' in self.df.columns):
                derivativeText = row['Aandelenoptie programma']
                if (derivativeText == 'Ja'):
                    derivative = True
                else:
                    derivative = False
            if ('Prijs' in self.df.columns):
                price = row['Prijs']
            else:
                amount = 0
            if ('Aantal' in self.df.columns):
                amount = row['Aantal']

            else:
                amount = 0
            if ('Eenheid' in self.df.columns):
                currency = row['Eenheid']

            trans = Transaction(instrumentType, isin, transType, transDescr, derivative, price, amount, currency)
            self.transactions.append(trans)

    def getGeneralAttributes(self):
        for index, row in self.df.iterrows():
            if ('Meldingsplichtige' in self.df.columns):
                self.rptOwnerName = row['Meldingsplichtige']
            if ('Uitgevende instelling' in self.df.columns):
                self.issuerName = row['Uitgevende instelling']
            if ('LEI' in self.df.columns):
                self.lei = row['LEI']
            if ('Positie' in self.df.columns):
                self.position = row['Positie']
            if ('Transactiedatum' in self.df.columns):
                rawDate = row['Transactiedatum']
                rawDate = rawDate.replace("mei", "may")
                rawDate = rawDate.replace("okt", "oct")
                rawDate = rawDate.replace("mrt", "mar")
                d = datetime.datetime.strptime(rawDate, '%d %b %Y')
                self.date = d.strftime('%Y-%m-%d')
                dateLiteral = Literal(self.date, datatype=XSD.date)
                self.g.add((self.filingUri, DC.date, dateLiteral))

    def generateOwner(self):
        ownerUri = URIRef(ns + 'people/' + str(uuid.uuid4()))
        self.rptOwner = ownerUri
        self.g.add((ownerUri, RDF.type, FOAF.Person))
        if (self.rptOwnerName):
            self.g.add((ownerUri, FOAF.name, Literal(self.rptOwnerName)))

        self.generateMembership(ownerUri)

        self.g.add((self.filingUri, OFP.hasRptOwner, ownerUri))

    def generateMembership(self, ownerUri):
        membership = URIRef(ns + 'memberships/' + str(uuid.uuid4()))

        self.g.add((membership, ORG.organization, self.issuer))

        self.g.add((membership, ORG.member, ownerUri))
        self.g.add((ownerUri, ORG.hasMember, membership))

        role = URIRef(ns + 'roles/' + str(uuid.uuid4()))
        self.g.add((role, RDF.type, OFP.Role))
        self.g.add((role, SKOS.prefLabel, Literal(self.position)))
        self.g.add((membership, ORG.role, role))

        self.g.add((membership, RDF.type, ORG.Membership))

    def generateIssuer(self):
        if (self.lei and self.lei != ''):
            issuerUri = URIRef(ns + 'companies/' + self.lei)
        else:
            issuerUri = URIRef(ns + 'companies/' + str(uuid.uuid4()))
        self.issuer = issuerUri

        if (self.issuerName):
            print('found a issuer with name ' + self.issuerName)
            self.g.add((issuerUri, SKOS.prefLabel, Literal(self.issuerName)))
            # Change preflabel of filing to include issuer name
            self.g.set((self.filingUri, SKOS.prefLabel, Literal("Ownership filing of " + self.issuerName)))

        if (self.lei and self.lei != ''):
            self.g.add((issuerUri, OFP.haslei, Literal(self.lei, datatype=OFP.lei)))

        self.g.add((issuerUri, RDF.type, OFP.PublicCompany))
        self.g.add((self.filingUri, OFP.issuedBy, issuerUri))

    def generateTransactions(self):
        for transaction in self.transactions:
            instrument = self.generateInstrument(transaction)
            transObj = URIRef(ns + 'transactions/' + str(uuid.uuid4()))
            self.g.add((transObj, RDF.type, OFP.Transaction))
            self.g.add((transObj, OFP.instrument, instrument))
            if (transaction.transDescr):
                self.g.add((transObj, SKOS.prefLabel, Literal(transaction.transDescr, datatype=XSD.string)))

            if (transaction.transType):
                if (transaction.transType == 'Verwerving'):
                    self.g.add((transObj, OFP.transactionType, Literal("Acquired")))
                elif (transaction.transType == 'Vervreemding'):
                    self.g.add((transObj, OFP.transactionType, Literal("Disposed")))

            # Add transdate
            if (self.date):
                self.g.add((transObj, OFP.transactionDate, Literal(self.date, datatype=XSD.date)))

            # volume
            self.g.add((transObj, OFP.amountOfInstruments, Literal(transaction.amount, datatype=XSD.decimal)))

            # price
            self.g.add((transObj, OFP.pricePerInstrument, Literal(transaction.price, datatype=XSD.decimal)))

            if (transaction.currency):
                self.g.add((transObj, OFP.currency, Literal(transaction.currency, datatype=OFP.currency)))

            self.generateOwnership(instrument)

            self.g.add((self.filingUri, OFP.reportsTransaction, transObj))

    def generateOwnership(self, instrument):
        updateOwnership = None
        ownershipType = None

        for ownership in self.ownerships:
            ownershipInstruments = self.g.objects(ownership, OFP.instrument)
            for ownershipInstrument in ownershipInstruments:
                if ownershipInstrument == instrument:
                    updateOwnership = ownership

        if (not updateOwnership):
            updateOwnership = URIRef(ns + 'ownerships/' + str(uuid.uuid4()))
            self.g.add((updateOwnership, RDF.type, OFP.Ownership))
            self.g.add((self.rptOwner, OFP.owns, updateOwnership))
            self.g.add((updateOwnership, OFP.instrument, instrument))
            self.ownerships.append(updateOwnership)
        return


    def generateInstrument(self, transaction):
        if (transaction.isin and transaction.isin != ''):
            instrument = URIRef(ns + 'instruments/' + str(transaction.isin))
        else:
            instrument = URIRef(ns + 'instruments/' + str(uuid.uuid4()))
        self.g.add((instrument, RDF.type, OFP.Instrument))
        if transaction.instrumentType:
            self.g.add((instrument, SKOS.prefLabel, Literal(transaction.instrumentType, datatype=XSD.string)))
        if (transaction.isin and transaction.isin != ''):
            self.g.add((instrument, OFP.hasisin, Literal(transaction.isin, datatype=OFP.isin)))

        self.g.add((instrument, OFP.underlyingCompany, self.issuer))
        return instrument

    def initGraph(self):
        self.g = Graph()
        self.g.bind("skos", SKOS)
        self.g.bind("foaf", FOAF)
        self.g.bind("dc", DC)
        self.g.bind("org", ORG)
        self.g.bind("ofp", OFP)
        self.g.bind("locn", LOCN)

    def generateBase(self):
        # Filing and metadata
        self.g.add((self.filingUri, RDF.type, OwnershipFiling))
        self.g.add((self.filingUri, SKOS.prefLabel, Literal("Ownership filing")))
        self.g.add((self.filingUri, SKOS.altLabel, Literal(str(self.id))))

        # Publisher
        AfmReg = URIRef(ns + 'regulators/afm')
        self.g.add((AfmReg, SKOS.prefLabel, Literal("Autoriteit Financiële Markten")))
        self.g.add((AfmReg, SKOS.prefLabel, Literal("Autoriteit Financiële Markten")))
        self.g.add((AfmReg, RDF.type, Regulator))
        self.g.add((self.filingUri, DC.publisher, AfmReg))

        # Filing type
        self.g.add((self.filingUri, DC.type, URIRef(ns + '/afmfilingtypes/' + 'managerstransactions')))
        self.g.add((URIRef(ns + '/afmfilingtypes/' + 'managerstransactions'), SKOS.prefLabel, Literal("Mar 19 Manager Transaction")))
        self.g.add((URIRef(ns + '/afmfilingtypes/' + 'managerstransactions'), DC.description, Literal(
            "In this overview you can find transactions that have been performed by managers in shares or debt instruments of the issuer or derivatives or other financial instruments linked thereto. When a manager performs such a transaction this transaction must, in principle, be reported. Examples of transactions which a manager must report are: the receipt of employee options, sale or purchase of shares.")))

    def saveGraph(self, dir, rdfdir):
        print("saved RDF of str(self.id)")

        with open(rdfdir + str(self.id) + ".ttl", 'w') as f:
            f.write(self.g.serialize(format='turtle').decode("utf-8"))

        with open(dir + ("rdf" + str(self.id) + ".txt"), 'w') as f:
            f.write(self.g.serialize(format='turtle').decode("utf-8"))
        return





