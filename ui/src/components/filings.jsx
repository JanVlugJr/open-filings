import React, {Component} from 'react';
import Filing from "./filing";

class Filings extends Component {
    state = {
        filings: [
            { id: 1 },
            { id: 2 },
            { id: 3 },
            { id: 4 }
        ],
        filingUris: [],

        amount: 10
    };

    componentDidMount(){

        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        console.log("1")
        fetch("http://localhost:8081/filings/latest/10", requestOptions)
            .then(response => this.setState({
                filingUris: response
            }))
            .then(response => console.log(response.text()))
            .catch(error => console.log('error', error));
        //
        // this.getAsyncFìlings().then(result => {console.log(JSON.stringify(result));});
    }

    async getAsyncFìlings() {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };

        let response = await  fetch("http://localhost:8081/filings/latest/10", requestOptions);
        let filings = await response.json();
        return filings;
    }

    getLatestFilings() {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        console.log("1")
        fetch("http://localhost:8081/filings/latest/10", requestOptions)
            .then(response => this.setState({
                filingUris: response.json()
            }))
            .then(response => console.log(response.text()))
            .catch(error => console.log('error', error));
    }


    render() {
        console.log(this.state.filingUris)
        return (
            <div className="filing-container">
                <ul>
                    {this.state.filingUris.filings.map((item, i) => {
                        return <li key={i}>{item}</li>
                    })}
                </ul>
                {/*{this.state.filingUris.map(item => (*/}
                {/*    <div>*/}
                {/*        {item.hit[0]}*/}
                {/*    </div>*/}
                {/*))}*/}
                {/*{this.state.filings.map(filing => <Filing key={filing.id}/>)}*/}
            </div>
        );
    }
}

export default Filings;