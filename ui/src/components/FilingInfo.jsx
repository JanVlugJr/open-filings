import React, {Component} from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import {ListGroup, ListGroupItem} from "react-bootstrap";


class FilingInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            filingInfo: []
        };
    }

    getOwner(){
        var owner;
        {this.state.filingInfo.reportingOwners.map(f => (this.setState(
            owner = f.name
        )))}
        return owner;
    }

    getInfo(uri) {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow',
            headers: { 'uri': uri}
        };
        console.log("1")
        fetch("http://localhost:8080/filings/blabla", requestOptions)
            .then(result => result.json())
            .then(result => {
                console.log(result) // works
                this.setState({
                    isLoaded: true,
                    filingInfo: result
                })
            })
            .catch(err => { console.log(err);
            })
        console.log(this.state.filingInfo)
    }


    componentDidMount() {
        this.getInfo('https://www.sec.gov//Archives/edgar/data/1403530/0000899243-21-028012-index.html');
    }


    render() {
        const {filingInfo} = this.state;
        if (!this.state.isLoaded) {
            return (<div>loading</div>
            )}

        return (
            <div>
                <Container fluid>
                    <Row>
                        <Card className="text-center" style={{width: '40rem' }}>
                            <Card.Header>{this.state.filingInfo.filingLabel}</Card.Header>
                            <Card.Body>
                                <Card.Title>{filingInfo.issuerLabel}</Card.Title>
                                <Card.Text>
                                    Aquisition of "Class A Common Share" on {filingInfo.date} by <a href="#">Oaktree Capital Group, LLC</a>.
                                </Card.Text>
                            </Card.Body>
                            <ListGroup className="list-group-flush">
                                <ListGroupItem>Regulator: <a href="#">SEC</a></ListGroupItem>
                                <ListGroupItem>Price per share: 16.67$</ListGroupItem>
                                <ListGroupItem>Amount of instruments: 1230</ListGroupItem>
                            </ListGroup>
                            <Card.Body>
                                <Card.Link href="#">Original PDF</Card.Link>
                                <Card.Link href="#">Original HTML</Card.Link>
                                <ListGroupItem><div><Button variant="primary">See detailed transactions</Button></div></ListGroupItem>

                            </Card.Body>

                        </Card>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default FilingInfo;