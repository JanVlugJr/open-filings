import React, { Component } from 'react';
import GetLatestFilings from "./GetLatestFilings";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import FilingInfo from "./FilingInfo";


class FilingContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            filingInfo: []
        };
        this.onClick= this.onClick.bind(this);

    }

    getInfo(uri) {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow',
            headers: { 'uri': uri}
        };
        console.log("1")
        fetch("http://localhost:8080/filings/blabla", requestOptions)
            .then(result => result.json())
            .then(result => {
                console.log(result) // works
                this.setState({
                    isLoaded: true,
                    filingInfo: result
                })
            })
            .catch(err => { console.log(err);
            })
        console.log(this.state.filingInfo)
    }

    getOwner(){
        var owner;
        {this.state.filingInfo.reportingOwners.map(f => (this.setState(
            owner = f.name
        )))}
            return owner;
    }

    componentDidMount() {
        this.getInfo(this.props.uri);
    }

    onClick() {
        this.setState({
            start: false,
        });
    }

    render() {
        return (
            <div>
                <Card style={{ width: '40rem' }}>
                    <Card.Body>
                        <Card.Title>{this.state.filingInfo.filingLabel}</Card.Title>
                        <Card.Text>
                            On {this.state.filingInfo.date}
                            <a href={this.props.uri}>Original Filing</a>
                        </Card.Text>
                        <Button onClick={this.onClick} variant="primary">View filing</Button>
                    </Card.Body>
                </Card>
            </div>
        )
    }
}

export default FilingContainer;