'use strict';
import React from 'react';
import {Component} from "react";

import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import Filing from "./filing";
import Filings from "./filings";
import GetLatestFilings from "./GetLatestFilings";
import FilingInfo from "./FilingInfo";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            start: false,
        };
    }

    clickBtn = e => {
        this.setState({
            start: +e.target.value
        });
    };


    render() {
        return (
            <div className="App">
                <h1 className="latest-filings">Latest Filings</h1>
                {(this.state.start) ? <GetLatestFilings clickBtn={this.clickBtn}/> : <FilingInfo />}

                <br/>
            </div>
        );
    }
}
export default App;