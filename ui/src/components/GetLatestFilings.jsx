import React, { Component } from 'react';
import FilingContainer from './FilingContainer';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';


class GetLatestFilings extends Component {
    constructor(props){
        super(props);
        this.state = {
            isLoaded: false,
            filingUris: []
        };
    }

    componentDidMount() {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };
        console.log("1")
        fetch("http://localhost:8080/filings/latest/10", requestOptions)
            .then(result => result.json())
            .then(result => {
                console.log(result) // works
                this.setState({
                    isLoaded: true,
                    filingUris: result
                })
            })
            .catch(err => { console.log(err);
            })
        console.log(this.state.filingUris)
    }

    render() {

        const {isLoaded, filingUris} = this.state;
        if(!isLoaded){
            return <div>Loading ...</div>
        }
        return (
            <div>
                <p>loaded</p>
                    {this.state.filingUris.filings.map(f => (
                        <div>
                            <FilingContainer uri={f}/>
                        </div>
                    ))}
            </div>

        );
    }
}

export default GetLatestFilings;