import React, {Component} from "react";
import Card from 'react-bootstrap/Button';


class Filing extends Component {
    state = {
        count: 0
    }
    styles = {
        fontSize: 50,
        fontWeight: 'bold',
        color: 'black'
    }
    render() {
        return (<React.Fragment>
            <span style={this.styles} className="badge badge-warning m-2">{this.state.count}</span>
            <button className="btn btn-secondary btn-sm">Increment</button>
        </React.Fragment>);
    }
}
export default Filing;