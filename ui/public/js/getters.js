class Getters {
    getFiling(uri) {
        let response = fetch("http://localhost/filings/" + uri, {"method": "GET", headers: {uri: uri}}, )

        if (response.ok) {
            let json = response.json()
            return json;
        } else {
            alert("HTTP-error: " + response.status)
        }

    }


    getCompany(uri) {
        let response = fetch("http://localhost/x/companies/" + uri, {"method": "GET"})

        if (response.ok) {
            let json = response.json()
        } else {
            alert("HTTP-error: " + response.status)
        }
    }

    getPerson(uri) {
        let response = fetch("http://localhost/x/people/" + uri, {"method": "GET"})

        if (response.ok) {
            let json = response.json()
        } else {
            alert("HTTP-error: " + response.status)
        }
    }

    getRandomFilings(uri, amount) {
        let response = fetch("http://localhost/x/randomfilings/" + uri, {"method": "GET",
            'headers': {
                'Amount': amount.str()
            }
        })

        if (response.ok) {
            let json = response.json()
        } else {
            alert("HTTP-error: " + response.status)
        }
    }

    getLatestFilings(uri, amount) {
        let response = fetch("http://localhost/x/latestfilings/" + uri, {"method": "GET",
            'headers': {
                'Amount': amount.str()
            }
        })

        if (response.ok) {
            let json = response.json()
        } else {
            alert("HTTP-error: " + response.status)
        }
    }

}