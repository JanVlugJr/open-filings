package org.openfilings.controllers;

import org.json.simple.JSONObject;
import org.openfilings.services.CompanyService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/companies")
public class CompanyController {
	
	CompanyService companyService = new CompanyService();

    @GetMapping("/{iri}")
    public JSONObject getCompany(@PathVariable String iri) {
        return companyService.getCompanyJson(iri);
    }
    
}
