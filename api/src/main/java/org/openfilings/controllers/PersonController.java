package org.openfilings.controllers;

import org.json.simple.JSONObject;
import org.openfilings.services.PersonService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/people")
public class PersonController {
	
	PersonService personService = new PersonService();

    @GetMapping("/{iri}")
    public JSONObject getperson(@PathVariable String iri) {
        return personService.getPersonJson(iri);
    }
    
}
