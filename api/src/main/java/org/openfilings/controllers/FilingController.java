package org.openfilings.controllers;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.openfilings.services.FilingService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/filings")
public class FilingController {
	
	FilingService filingService = new FilingService();

	@CrossOrigin
    @GetMapping("/{iri}")
    public JSONObject getFiling(HttpServletRequest request) {
    	String uri = request.getHeader("uri");
        return filingService.getFilingJson(uri);
    }

    @CrossOrigin
    @GetMapping("/random/{amount}")
    public JSONObject getRandomFilings(@PathVariable int amount) {
        return filingService.getRandomFilings(amount);
    }
    
    @CrossOrigin
    @GetMapping("/latest/{amount}")
    public JSONObject getLatestFilings(@PathVariable int amount) {
        return filingService.getLatestFilings(amount);
    }
    
    
}