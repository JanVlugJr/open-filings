package org.openfilings;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OfpApplication {

	public static void main(String[] args) {
		SpringApplication.run(OfpApplication.class, args);
	}

}
