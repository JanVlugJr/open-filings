package org.openfilings.services;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.OWL;
import org.springframework.stereotype.Component;

// https://github.com/apache/jena/blob/main/jena-rdfconnection/src/main/java/org/apache/jena/rdfconnection/examples/RDFConnectionExample4.java
// https://github.com/apache/jena/blob/main/jena-rdfconnection/src/main/java/org/apache/jena/rdfconnection/examples/RDFConnectionExample6.java



@Component
public class OfpConnector {


	    private ResultSet rs;

	    Model model;

	    public OfpConnector() {
	        Dataset dataset = RDFDataMgr.loadDataset("/Users/jan/Documents/ComputerScience/scriptie/ofp/kb.ttl");
	        this.model = dataset.getDefaultModel();
	        model.setNsPrefix("owl", OWL.getURI());
	    }



	    public ResultSet selectQuery(String queryStr) {
	        System.out.println(queryStr);
	        Query query = QueryFactory.create(queryStr);
	        try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {
	            ResultSet results = qexec.execSelect();
	            ResultSet rs = ResultSetFactory.copyResults(results) ;
	            return rs ;
	        }
	    }
	    
	    public Model describeQuery(String queryStr) {
	    	System.out.println(queryStr);
	    	Query query = QueryFactory.create(queryStr);
	    	try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {
	            Model model = qexec.execDescribe();
	            return model ;
	        }
	    }

}
