package org.openfilings.services;

import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionFactory;
import org.apache.jena.rdfconnection.RDFConnectionFuseki;
import org.apache.jena.rdfconnection.RDFConnectionRemoteBuilder;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class ResultMapperLocal {
	OfpConnector ofpConnection = new OfpConnector();
	
	public JSONObject mapFiling(Model filingModel) {
		JSONObject filingJson = populateObject(filingModel);

		return filingJson;
	}
	
	private JSONObject populateObject(Model filingModel) {
		JSONObject filingJson = new JSONObject();
		
		ParameterizedSparqlString pss;
		System.out.println("mapping filing:" + filingModel.toString());
		
		// set filingLabel
		ResultSet results = selectQuery(filingModel, "SELECT ?filingLabel WHERE {?filing skos:prefLabel ?filingLabel}");
		for ( ; results.hasNext() ; ) {
			QuerySolution soln = results.nextSolution() ;
		    Literal l = soln.getLiteral("?filingLabel") ;
		    filingJson.put("filingLabel", l.getString());
		}

		// set date
		results = selectQuery(filingModel, "SELECT ?date WHERE {?filing dc:date ?date}");
		for ( ; results.hasNext() ; ) {
			QuerySolution soln = results.nextSolution() ;
		    Literal l = soln.getLiteral("?date") ;
		    filingJson.put("date", l.getString());
		}

		// set webVersion
		results = selectQuery(filingModel, "SELECT ?webVersion WHERE {?filing dc:hasVersion ?webVersion}");
		if ( results.hasNext() ) {
			QuerySolution soln = results.nextSolution() ;
		    Resource r = soln.getResource("?webVersion") ;
		    filingJson.put("webVersion", r.toString());
		}

		// set regulator
		Resource regulator = null;
		results = selectQuery(filingModel, "SELECT ?regulator WHERE {?filing dc:publisher ?regulator}");
		for ( ; results.hasNext() ; ) {
			QuerySolution soln = results.nextSolution() ;
		    Resource r = soln.getResource("?regulator") ;
		    regulator = r;
		    filingJson.put("regulator", r.toString());
		}
		
		
		// set regulatorName
		if (regulator != null) {
			String queryString = ("PREFIX skos: <http://www.w3.org/2004/02/skos/core#> SELECT ?label WHERE { <" + regulator.toString() + "> skos:prefLabel ?label.}" );
		    System.out.println(queryString);
		  
		    RDFConnectionRemoteBuilder builder = RDFConnectionFuseki.create()
	                .destination("http://localhost:3030/ofplinked/sparql");
		    try ( RDFConnection conn = (RDFConnectionFuseki)builder.build()) {
		    	conn.begin(ReadWrite.WRITE) ;
		        ResultSet rs = conn.query(queryString).execSelect();
		        for ( ; rs.hasNext() ; ) {
		        	QuerySolution soln = rs.nextSolution() ;
		        	Literal l = soln.getLiteral("?label") ;
		        	filingJson.put("regulatorName", l.getString());
		        }
		        conn.close();
		    }	           
		}

		// set rptOwner - needs to be loop -- handled in seperate method
		JSONObject owners = this.getOwners(filingModel);
		filingJson.put("reportingOwners", owners);
		

		// set issuer
		Resource issuer = null;
		results = selectQuery(filingModel, "SELECT ?issuer WHERE {?filing ofp:issuedBy ?issuer}");
		for ( ; results.hasNext() ; ) {
			QuerySolution soln = results.nextSolution() ;
		    Resource r = soln.getResource("?issuer") ;
		    issuer = r;
		    filingJson.put("issuer", r.toString());
		}
		
		// set issuerLabel
// TODO - here is a problem
		if (issuer != null) {
			String queryString = ("PREFIX skos: <http://www.w3.org/2004/02/skos/core#> SELECT ?label WHERE { <" + issuer.toString() + "> skos:prefLabel ?label.}" );
		    System.out.println(queryString);
		    
		    try (  RDFConnection conn = RDFConnectionFactory.connect("http://localhost:3030/ofplinked/sparql", null, null)) {
		    	System.out.println("I tried");
		    	conn.begin(ReadWrite.READ) ;
		      //  ResultSet rs = conn.query(queryString).execSelect();
		    	//ResultSet rs;
		    	
		    	Query qExecuteQuery = QueryFactory.create(queryString);
		    	QueryExecution qexec = QueryExecutionFactory.sparqlService("http://localhost:3030/ofplinked/sparql", qExecuteQuery);
//		    	qexec.execSelect();
//		    	conn.querySelect(queryString, (qs)->{
//	                  Literal label = qs.getLiteral("?label") ;
//	                  filingJson.put("issuerLabel", label.getString());
//	              }) ;
		    	System.out.println("no success");
//		        if (rs.hasNext()) {
//		        	QuerySolution soln = rs.nextSolution() ;
//		        	Literal l = soln.getLiteral("?label") ;
//		        	
//		        }
		        conn.close();
		    }	           
		}
		
		// get transaction  - needs to be loop
		results = selectQuery(filingModel, "SELECT ?filingLabel WHERE {?filing skos:prefLabel ?filingLabel}");
		for ( ; results.hasNext() ; ) {
			QuerySolution soln = results.nextSolution() ;
		    Literal l = soln.getLiteral("?filingLabel") ;
		    filingJson.put("filingLabel", l.getString());
		}


		
		return filingJson;
	}

}
