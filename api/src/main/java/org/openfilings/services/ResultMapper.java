package org.openfilings.services;

import java.io.ByteArrayOutputStream;

import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFactory;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionFactory;
import org.apache.jena.rdfconnection.RDFConnectionFuseki;
import org.apache.jena.rdfconnection.RDFConnectionRemoteBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

@Service
public class ResultMapper {
	
	OfpConnector ofpConnection = new OfpConnector();


	public JSONObject mapFiling(Model filingModel) {
		JSONObject filingJson = populateObject(filingModel);

		return filingJson;
	}
	
	private JSONObject populateObject(Model filingModel) {
		JSONObject filingJson = new JSONObject();
		
		ParameterizedSparqlString pss;
		
		System.out.println(filingModel.toString());
		
		
		// set filingLabel
		ResultSet results = selectQueryOnModel(filingModel, "SELECT ?filingLabel WHERE {?filing skos:prefLabel ?filingLabel}");
		for ( ; results.hasNext() ; ) {
			QuerySolution soln = results.nextSolution() ;
		    Literal l = soln.getLiteral("?filingLabel") ;
		    filingJson.put("filingLabel", l.getString());
		}

		// set date
		results = selectQueryOnModel(filingModel, "SELECT ?date WHERE {?filing dc:date ?date}");
		for ( ; results.hasNext() ; ) {
			QuerySolution soln = results.nextSolution() ;
		    Literal l = soln.getLiteral("?date") ;
		    filingJson.put("date", l.getString());
		}

		// set webVersion
		results = selectQueryOnModel(filingModel, "SELECT ?webVersion WHERE {?filing dc:hasVersion ?webVersion}");
		if ( results.hasNext() ) {
			QuerySolution soln = results.nextSolution() ;
		    Resource r = soln.getResource("?webVersion") ;
		    filingJson.put("webVersion", r.toString());
		}

		// set regulator
		Resource regulator = null;
		results = selectQueryOnModel(filingModel, "SELECT ?regulator WHERE {?filing dc:publisher ?regulator}");
		for ( ; results.hasNext() ; ) {
			QuerySolution soln = results.nextSolution() ;
		    Resource r = soln.getResource("?regulator") ;
		    regulator = r;
		    filingJson.put("regulator", r.toString());
		}
		
		
		// set regulatorName
		if (regulator != null) {
			String queryString = ("PREFIX skos: <http://www.w3.org/2004/02/skos/core#> SELECT ?label WHERE { <" + regulator.toString() + "> skos:prefLabel ?label}" );
		    System.out.println(queryString);
		  
		
		    ResultSet rs = ofpConnection.selectQuery(queryString);
		        for ( ; rs.hasNext() ; ) {
		        	QuerySolution soln = rs.nextSolution() ;
		        	Literal l = soln.getLiteral("?label") ;
		        	filingJson.put("regulatorName", l.getString());
		        }
		    }	           
		

		// set rptOwner - needs to be loop -- handled in seperate method
		JSONObject owners = this.getOwners(filingModel);
		filingJson.put("reportingOwners", owners);
		

		// TODO - this + the next if should be fixed so that a label is added for every issuer
		// set issuer
		Resource issuer = null;
		results = selectQueryOnModel(filingModel, "SELECT ?issuer WHERE {?filing ofp:issuedBy ?issuer}");
		for ( ; results.hasNext() ; ) {
			QuerySolution soln = results.nextSolution() ;
		    Resource r = soln.getResource("?issuer") ;
		    issuer = r; // should be added to array.
		    filingJson.put("issuer", r.toString());
		}
		
		// set issuerLabel
// TODO - here is a problem
		if (issuer != null) {
			String queryString = ("PREFIX skos: <http://www.w3.org/2004/02/skos/core#> SELECT ?label WHERE { <" + issuer.toString() + "> skos:prefLabel ?label.}" );
		    System.out.println(queryString);
		    
		    System.out.println("Trying to set issuer label");
		    ResultSet rs = ofpConnection.selectQuery(queryString);
		    
		    for ( ; rs.hasNext() ; ) {
	        	QuerySolution soln = rs.nextSolution() ;
	        	Literal l = soln.getLiteral("?label") ;
	        	filingJson.put("issuerLabel", l.getString());
	        }    
		}
		
		// get transaction URIs as JSONArray
		filingJson.put("transactions", getTransactions(filingModel));


		
		return filingJson;
	}
	
	private ParameterizedSparqlString getPrefixedPss() {
		 ParameterizedSparqlString pss = new ParameterizedSparqlString();
	     	pss.setNsPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
	        pss.setNsPrefix("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
	        pss.setNsPrefix("dbo", "http://dbpedia.org/ontology/");
	        pss.setNsPrefix("foaf", "http://xmlns.com/foaf/0.1/");
	        pss.setNsPrefix("ofp", "http://www.openfilings.org/ofp/2021/5/");
	        pss.setNsPrefix("skos", "http://www.w3.org/2004/02/skos/core#");
	        pss.setNsPrefix("dc", "http://purl.org/dc/elements/1.1/");
	    return pss;
	}
	
	private ResultSet selectQueryOnModel(Model model, String queryString) {
		ParameterizedSparqlString pss = getPrefixedPss();
		pss.setCommandText(queryString);
		try (QueryExecution qexec = QueryExecutionFactory.create(pss.toString(), model)) {
			ResultSet results = qexec.execSelect() ;
		    return ResultSetFactory.copyResults(results) ;
		}	
	}
	
	private JSONArray getTransactions(Model model) {
		JSONArray transactions = new JSONArray();
		
		ResultSet results = selectQueryOnModel(model, "SELECT ?transaction WHERE {?filing ofp:reportsTransaction ?transaction}");
		
		for ( ; results.hasNext() ; ) {
			QuerySolution soln = results.nextSolution() ;
		    Resource trans = soln.getResource("?transaction") ;
		    transactions.add(trans.toString());
		}
		
		return transactions;
	}
	
	
	// TODO - Gets only one!
	private JSONObject getOwners(Model model) {
		JSONObject owners = new JSONObject();
		
		// find all ownerURIs with query
		// for every owner find name and add


		ResultSet results = selectQueryOnModel(model, "SELECT ?rptOwner WHERE {?filing ofp:hasRptOwner ?rptOwner}");
		for (int i = 0 ; results.hasNext() && i < 4 ; i++ ) {
			QuerySolution soln = results.nextSolution() ;
			Resource rptOwner = soln.getResource("?rptOwner") ;

			JSONObject tmpOwner = new JSONObject();
			tmpOwner.put("uri", rptOwner.toString());

			System.out.println("owner result");
			String ownerName = "";
			ResultSet rs = ofpConnection.selectQuery("PREFIX foaf: <http://xmlns.com/foaf/0.1/> SELECT ?name WHERE { <" + rptOwner.toString() + "> foaf:name ?name.}");
			if (rs.hasNext()) {
				QuerySolution soln2 = rs.nextSolution() ;
				Literal l = soln2.getLiteral("?name") ;
				ownerName = l.getString();
			}

			tmpOwner.put("name", ownerName);


			JSONArray ownerArray = new JSONArray();
			ownerArray.add(tmpOwner);
			owners.put("reportingOwner", ownerArray);
		}


		return owners;
	}
}


//PREFIX ofp: <http://www.openfilings.org/ofp/2021/5/>
//PREFIX rfd: <http://com.intrinsec//ontology#>
//PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
//PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
//PREFIX dc: <http://purl.org/dc/elements/1.1/>
//PREFIX foaf: <http://xmlns.com/foaf/0.1/>
//SELECT ?filingType ?filingLabel ?date ?webVersion ?regulatorName ?rptOwner ?rptOwnerName ?issuer ?issuerLabel ?transaction
//WHERE
//{ 
//	BIND ( <https://www.unternehmensregister.de/ureg/result.html;jsessionid=56FCF942C5A2B49F01D60EA60BF11750.web04-1?submitaction=showPrintDoc&id=19913255&pid=0> as ?filing) .
//	?filing dc:type ?filingType .
//	?filing skos:prefLabel ?filingLabel .
//  	?filing dc:date ?date .
//  	?filing dc:hasVersion ?webVersion .
//  	?filing dc:publisher ?regulator .
//  	?regulator skos:prefLabel ?regulatorName .
//  	?filing ofp:hasRptOwner ?rptOwner .
//  	?rptOwner foaf:name ?rptOwnerName .
//  	?filing ofp:hasIssuer ?issuer .
//  	?issuer skos:prefLabel ?issuerLabel .
//  	?filing ofp:reportsTransaction ?transaction .
//} LIMIT 10