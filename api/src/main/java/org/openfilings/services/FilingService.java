package org.openfilings.services;

import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdfconnection.RDFConnection;
import org.apache.jena.rdfconnection.RDFConnectionFuseki;
import org.apache.jena.rdfconnection.RDFConnectionRemoteBuilder;
import org.apache.jena.system.Txn;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.openfilings.ReadPropertyFile;
import org.openfilings.entities.Filing;
import org.springframework.stereotype.Service;


import java.util.List;

import javax.management.Query;

@Service
public class FilingService {
	
	ResultMapper resultMapper = new ResultMapper();
	OfpConnector ofpConnection = new OfpConnector();

	
	public JSONObject getFilingJson(String uri) {
		
		
				
		JSONObject filing;
	    ParameterizedSparqlString pss = getPrefixedPss();
	    String queryString = ("DESCRIBE <" + uri + ">" );
	    System.out.println(queryString);
	    
	    
	    Model model = ofpConnection.describeQuery(queryString);
	    

	    System.out.println("going to result mapper");
		filing = resultMapper.mapFiling(model);

		return filing;
	}
	
	public JSONObject getRandomFilings(int amount) {
		JSONObject filings = new JSONObject();
		
		return filings;
	}
	
	public JSONObject getLatestFilings(int amount) {
		JSONObject filings = new JSONObject();
		ResultSet resultsCopy;
	    String queryString = ("PREFIX ofp: <http://www.openfilings.org/ofp/2021/5/> PREFIX dc: <http://purl.org/dc/elements/1.1/> PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> SELECT DISTINCT ?filing WHERE {?filing rdf:type ofp:OwnershipFiling . ?filing dc:date ?date . } ORDER BY DESC(?date) LIMIT " + String.valueOf(amount) );
	    System.out.println(queryString);
	    

	    JSONArray filingsArray = new JSONArray();
	    ResultSet rs = ofpConnection.selectQuery(queryString);
	    for (int i = 0 ; rs.hasNext() ; i++ ) {
        	QuerySolution soln = rs.nextSolution() ;
        	Resource filing = soln.getResource("?filing");
        	filingsArray.add(filing.toString());
        	
        }
	    
	
            filings.put("filings", filingsArray);
//            conn.close();

		return filings;
	}
	
	
	private ParameterizedSparqlString getPrefixedPss() {
		 ParameterizedSparqlString pss = new ParameterizedSparqlString();
	     	pss.setNsPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
	        pss.setNsPrefix("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
	        pss.setNsPrefix("dbo", "http://dbpedia.org/ontology/");
	        pss.setNsPrefix("foaf", "http://xmlns.com/foaf/0.1/");
	        pss.setNsPrefix("ofp", "http://www.openfilings.org/ofp/2021/5/");
	        pss.setNsPrefix("skos", "http://www.w3.org/2004/02/skos/core#");
	    return pss;
	}
	
	private RDFConnection getOfpConnection() {
		ReadPropertyFile prop = new ReadPropertyFile();
		RDFConnectionRemoteBuilder builder = RDFConnectionFuseki.create()
                .destination("http://localhost:3030/ofplinked/sparql"); // TODO - TAKE FROM CONFIG FILE

        // In this variation, a connection is built each time.
        try ( RDFConnectionFuseki conn = (RDFConnectionFuseki)builder.build() ) {
        	return conn;
        }
	}
}


