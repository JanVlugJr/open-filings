package org.openfilings;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadPropertyFile {
	
	Properties prop;
	
	public ReadPropertyFile() {
		this.prop=new Properties();
		try {
			FileInputStream ip= new FileInputStream("/ofp/src/main/resources/config.properties");
			this.prop.load(ip);
		} 
		catch (Exception e) {
			
		}
		
	}

	public String getSparqlEndpoint() {
		return prop.getProperty("sparqlendpoint");
	}

}
